### Contenu
- Le dossier /data contient:
    - le fichier data, contenant les données d'entrainement du modèle
    - le fichier data_matériaux, contenant les compositions types de quelques matériaux, avec des valeurs choisies arbitrairement pour la taille médiane des grains et le taux de charge
- Le dossier /models contient les modèles utilisés pour faire les prédictions
- Le dossier /prédictions contiendra le résultat des prédictions réalisées
- Le dossier /scaler contient le fichier qui permettra de normaliser les données d'entrée à la bonne échelle pour pouvoir faire de bonnes prédictions
- Le dossier /script contient les différents scripts à exécuter

### Utilisation:
Exécuter le script "predict.py" du dossier "scripts" avec l'argument --predict pour fournir un chemin vers les données à prédire.
Ensuite, le script raw_predict.py permet d'obtenir un exemple de résultats en valeurs réelles à partir d'un PHBV type.

