from sklearn.svm import LinearSVC
from sklearn import tree
from sklearn import svm
from sklearn import linear_model
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier, GradientBoostingRegressor, RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier, KNeighborsRegressor
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.datasets import make_classification, make_regression
from sklearn.experimental import enable_halving_search_cv  
from sklearn.model_selection import HalvingGridSearchCV
from sklearn.model_selection import LeaveOneOut
from sklearn.preprocessing import FunctionTransformer
from statistics import mean, stdev
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import train_test_split
import shap
import numpy as np
from joblib import dump
import pandas as pd
from time import time
from sklearn.preprocessing import LabelEncoder
from xgboost import XGBClassifier

inputs = ['granulo_D50','compo_lignine','compo_cellulose','compo_hemicell','compo_ash','tdc_taux']
outputs = ['meca_strainAtBreak','meca_stressAtBreak','meca_youngModulus','permea_h2o','TGA_onset','TGA_degradation','DSC_crystallization','DSC_meltingPoint']
outliers = ['PHBV2-CF(30%) film', 'PHBV-CM(30%) film','PHBV2-PF(15%) film','PHBV2-SF(15%) film']

d_discret = {
        'meca_strainAtBreak': [0.2, 0.5, 0.8, 1.08], # tension reached at the breaking point in a tensile test
        'meca_stressAtBreak': [0, 0.8, 1.32], # stress reached at the breaking point in a tensile test
        'meca_youngModulus': [0.25, 0.8, 1, 1.5], # modulus of elasticity, y = stress/strain
        'permea_h2o': [0.3, 1, 2, 17], # water vapour permeability
        'TGA_onset': [0.3, 0.95, 0.98, 1.04], # onset degradation temperature, écart faible entre les valeurs, 
        'TGA_degradation': [0.73, 0.9, 1.8], # temperature at the maximal rate of degradation)
        'DSC_crystallization': [0.9, 0.98, 1.02], #crystallization temperature
        'DSC_meltingPoint': [0.96, 0.98, 1.1], # melting temperature
    }

classifiers={
    'random_forest_classifier': RandomForestClassifier,
    'svm_classifier': svm.SVC,
    'nearest_neighbours_classifier': KNeighborsClassifier,
    'decision_tree_classifier': tree.DecisionTreeClassifier,
    'gradient_boosted_tree_classifier': GradientBoostingClassifier,
    'neural_network_classifier': MLPClassifier,
}

standard_scaler = ('standard_scaling', StandardScaler())

def standardize_auto(data, inputs):
    scaler = StandardScaler()
    data[inputs]=pd.DataFrame(scaler.fit_transform(data[inputs]), columns = data[inputs].columns, index=data.index)
    return(data)

def transform( bins, l_att ):
    labels = [ f"{chr(i+97)}_){round(bins[i],1)}-{round(bins[i+1],1)}]" for i in range(len(bins)-1) ]
    f = FunctionTransformer(
                     pd.cut, kw_args={'bins': bins, 'labels': labels, 'retbins': False} )
    return( f.transform( l_att ) )

def discretize(data, outputs, discretization):
    data_dis = data[:]
    for output in outputs:
        data_dis[output] = transform(discretization[output], data[output])
    return(data_dis)

def detect_outlier(data, k, columns):
    outliers = []
    for column in columns:
        # outliers[column] = []
        col = data[column].dropna()

        # find q1 and q3 values
        q1, q9 = np.percentile(sorted(col), [10, 90])
    
        # compute IRQ
        iqr = q9 - q1
    
        # find lower and upper bounds
        lower_bound = q1 - (k * iqr)
        upper_bound = q9 + (k * iqr)
    
        # outliers = [x for x in data if x <= lower_bound or x >= upper_bound]
        col_outliers = col[(col <= lower_bound) | (col >= upper_bound)].index.tolist()
        for item in col_outliers:
            outliers.append(item)

    return(outliers)

def discretize(data, variables, discretization):
    data_dis = data[:]
    for variable in variables:
        data_dis[variable] = transform(discretization[variable], data[variable])
    return(data_dis)

def standardize_auto(data, inputs, scaler_filename=None):
    data_copy = data.copy()
    scaler = StandardScaler()
    data_copy[inputs]=pd.DataFrame(scaler.fit_transform(data_copy[inputs]), columns = data_copy[inputs].columns, index=data.index)
    if scaler_filename:
        dump(scaler, scaler_filename)
    return(data_copy)

def evaluate_model(model_name, param_grids, data, inputs, outputs):
    start = time()
    pred_val = {}
    true_val = {}
    scores=[]

    best_model = hyperparameters_optimization(model_name, param_grids)

    # mdl = Pipeline([feature_selection, (model_name, best_model.best_estimator_)])

    for output in outputs:
        pred_val[output] = np.array([])
        true_val[output] = np.array([])
        y = data[output].dropna()
        X = data[inputs][data.index.isin(y.index)]
        
        if model_name == 'xgboost_classifier':
            le = LabelEncoder()
            y = pd.Series(le.fit_transform(y))
            best_model.best_params_["num_class"]=1


        mdl = all_models[model_name](**best_model.best_params_)

        for train, test in  LeaveOneOut().split(X):
            X_train, X_test = X.iloc[train], X.iloc[test]
            y_train, y_test = y.iloc[train], y.iloc[test]
            mdl.fit(X_train,y_train)

            pred_val[output] = np.append(pred_val[output], mdl.predict(X_test))
            true_val[output] = np.append(true_val[output], y_test.to_numpy())

        if model_name == 'xgboost_classifier':
            pred_val[output] = le.inverse_transform(pred_val[output].astype(int))
            true_val[output] = le.inverse_transform(true_val[output].astype(int))

        # print(f'Predicted values for {output}: {pred_val[output]}')

        scores+=[f1_score(true_val[output], pred_val[output], average='weighted')]

    print(f"{model_name} done in {round(time()-start, 2)} seconds\n")
    return(scores, best_model.best_params_, pred_val, true_val)

def evaluate_model_old(model_name, param_grids, data, inputs, outputs, test_size, iterations):
    start = time()
    means=[]
    dev=[]
    pred_val = {}
    true_val = {}

    best_model = hyperparameters_optimization(model_name, param_grids)

    # mdl = Pipeline([feature_selection, (model_name, best_model.best_estimator_)])
    mdl = all_models[model_name](**best_model.best_params_)

    for output in outputs:
        pred_val[output] = np.array([])
        true_val[output] = np.array([])

        y = data[output].dropna()
        X = data[inputs][data.index.isin(y.index)]
        scores=[]
        
        for i in range (iterations):
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
            mdl.fit(X_train,y_train)

            pred_val[output] = np.append(pred_val[output], mdl.predict(X_test))
            true_val[output] = np.append(true_val[output], y_test.to_numpy())

            scores+=[f1_score(true_val[output], pred_val[output], average='weighted')]

        means+=[mean(scores)]
        dev+=[stdev(scores)]
    print(f"{model_name} done in {round(time()-start, 2)} seconds\n")
    return(means, dev, best_model.best_params_, pred_val, true_val)

def detect_outlier(data, k, columns):
    outliers = []
    for column in columns:
        # outliers[column] = []
        col = data[column].dropna()

        # find q1 and q3 values
        q1, q9 = np.percentile(sorted(col), [10, 90])
    
        # compute IRQ
        iqr = q9 - q1
    
        # find lower and upper bounds
        lower_bound = q1 - (k * iqr)
        upper_bound = q9 + (k * iqr)
    
        # outliers = [x for x in data if x <= lower_bound or x >= upper_bound]
        col_outliers = col[(col <= lower_bound) | (col >= upper_bound)].index.tolist()
        for item in col_outliers:
            outliers.append(item)

    return(outliers)


def hyperparameters_optimization(model_name, param_grids):
    start = time()
    param_grid = param_grids[model_name]
    base_estimator = all_models[model_name]()

    if 'classifier' in model_name:
        X, y = make_classification(n_samples=1000, random_state=0)
    elif 'regression' in model_name:
        X, y = make_regression(n_samples=1000, random_state=0)
    sh = HalvingGridSearchCV(base_estimator, param_grid, cv=5, factor=2, max_resources=30).fit(X, y)
    print(f"Best params: {sh.best_params_}, determined in {round(time()-start, 2)} seconds")
    return(sh)
    
def split(min,max,n): # génère une liste de n valeurs réparties entre min et max
    list = [min]
    step = (max-min)/(n-1) 
    for k in range(n-1):
        list+=[min+(k+1)*step]
    return(list)

default_params = {}

param_grids = {
    'random_forest_classifier': {
        'max_depth': [3, 5, 10],
        'min_samples_split': [2, 5, 10],
        'class_weight': ['balanced']
    },
    'svm_classifier': {
        'class_weight' : ['balanced'],
        'kernel': ['rbf','linear','poly','sigmoid'],
        'probability': [True]
    },
    'nearest_neighbours_classifier': {
        'n_neighbors': [3,5,7],
        'weights': ['uniform','distance'],
        'leaf_size': [10,30,50],
    },
    'decision_tree_classifier': {
        'class_weight': ['balanced'],
        'criterion': ['gini','entropy','log_loss']
    },
    'gradient_boosted_tree_classifier': {
        'criterion': ['friedman_mse','squared_error']
    },
    'neural_network_classifier': {
        'activation': ['identity','logistic','tanh','relu'],
        'solver': ['lbfgs','sgd','adam']
    },
    'xgboost_classifier': {
        # 'tree_method': ["hist"], 
        'enable_categorical': [True], 
        # 'device': ["cuda"]
    },
    'random_forest_regression': {
        'max_depth': [3, 5, 10],
        'min_samples_split': [2, 5, 10],
    },
    'linear_regression': {

    },
    'bayesian_ridge_regression': {
        'max_iter' : [20000],
    },
    'svm_regression': {
        'kernel': ['rbf','linear','poly','sigmoid'],
        'probability': [True]
    },
    'nearest_neighbours_regression': {

    },
    'decision_tree_regression': {

    },
    'gradient_boosted_tree_regression': {

    },
    'neural_network_regression': {
        'max_iter' : [20000],
    }
}

standard_scaler = ('standard_scaling', StandardScaler())

classifiers={
    'random_forest_classifier': RandomForestClassifier,
    'svm_classifier': svm.SVC,
    'nearest_neighbours_classifier': KNeighborsClassifier,
    'decision_tree_classifier': tree.DecisionTreeClassifier,
    'gradient_boosted_tree_classifier': GradientBoostingClassifier,
    # 'neural_network_classifier': MLPClassifier,
    'xgboost_classifier': XGBClassifier,
}

regressions={
   'random_forest_regression': RandomForestRegressor,
    'linear_regression': linear_model.LinearRegression,
    'bayesian_ridge_regression': linear_model.BayesianRidge,
    'svm_regression': svm.SVR,
    'nearest_neighbours_regression': KNeighborsRegressor,
    'decision_tree_regression': tree.DecisionTreeRegressor,
    'gradient_boosted_tree_regression': GradientBoostingRegressor,
    'neural_network_regression': MLPRegressor,
}

all_models = classifiers | regressions

explainers={
    'random_forest_classifier': shap.TreeExplainer,
    'RandomForestClassifier': shap.TreeExplainer,
    'svm_classifier': shap.KernelExplainer,
    'SVC': shap.KernelExplainer,
    'nearest_neighbours_classifier': shap.KernelExplainer,
    'KNeighborsClassifier': shap.KernelExplainer,
    'decision_tree_classifier': shap.TreeExplainer,
    'DecisionTreeClassifier': shap.TreeExplainer,
    'gradient_boosted_tree_classifier': shap.TreeExplainer,
    'GradientBoostingClassifier': shap.TreeExplainer,
    'XGBClassifier': shap.TreeExplainer,
    'neural_network_classifier': shap.KernelExplainer,
    'MLPClassifier': shap.KernelExplainer,
}
