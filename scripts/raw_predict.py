import pandas as pd
from pathlib import Path
import argparse

parent = Path(__file__).resolve().parent.parent

parser = argparse.ArgumentParser(description='Gives the raw values for the output variables predictions, based on the PHBV used in the ECOBIOCAP project. This script is meant to be used AFTER running predict.py')
parser.add_argument('--data', type=str, default=f"{parent}/predictions/predicted_data.csv", help='A path to the file containing the predicted data')
args = parser.parse_args()

def format(n):
    if n>=0.01:
        return(round(n,2))
    else:
        return(n)

data = pd.read_csv(args.data, sep=",", encoding="utf-8", index_col=0)

phbv = { # ici, ECOBIOCAP_PHBV
    "meca_strainAtBreak":2.29,
    "meca_stressAtBreak":39.2,	
    "meca_youngModulus":2.73,	
    "permea_h2o":9.72E-13,
    "TGA_onset":276,	
    "TGA_degradation":315,	
    "DSC_crystallization":121.5,	
    "DSC_meltingPoint":172.6
}

for output in phbv.keys():
    col=[]
    for ligne in data[output]:
        min = float(ligne.split("-")[0])
        max = float(ligne.split("-")[1])
        ligne = f"{format(min*phbv[output])}-{format(max*phbv[output])}"
        col.append(ligne)
    data[output]=col

data.to_csv(args.data[:args.data.rindex('/')+1]+"raw_"+args.data[args.data.rindex('/')+1:], index=True)