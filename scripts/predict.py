from joblib import load
import sys
import pandas as pd
from pathlib import Path
from extras import *
import argparse
import pickle

parent = Path(__file__).resolve().parent.parent

parser = argparse.ArgumentParser(description='Predict the outputs of the input file.')
parser.add_argument('--predict', type=str, default=f"{parent}/data/data.csv", help='A path to the file containing the data to predict')
parser.add_argument('--input_var', type=str, nargs='+', default=['granulo_D50','compo_lignine','compo_cellulose','compo_hemicell','compo_ash','tdc_taux'], help='A list of the input variables')
parser.add_argument('--output_path', type=str, default=f"{parent}/predictions", help="A path to the folder where the predictions will be stored")
parser.add_argument('--output_var', type=str, nargs='+', default=['meca_strainAtBreak','meca_stressAtBreak','meca_youngModulus','permea_h2o','TGA_onset','TGA_degradation','DSC_crystallization','DSC_meltingPoint'], help='A list of the output variables')
parser.add_argument('--models', type=str, default=f"{parent}/models", help='Path to the folder containing the models')
parser.add_argument('-v', '--verbose', action='store_true', help='Print more details about the predictions made')
args = parser.parse_args()

if not args.predict:
    sys.exit("Please provide data to predict with the --predict argument")

inputs = args.input_var

print("The given models were created with scikit-learn version 1.4.2. If you are using a different version, you might encounter errors")

data_pred = pd.read_csv(args.predict, sep=";", encoding="utf-8", index_col=0)
data_pred = data_pred[inputs]
data_pred = data_pred.drop(index=outliers)

for input in inputs:
    data_pred = data_pred.dropna(subset=[input])

scaler = load(f"{parent}/scalers/scaler.joblib")
data_pred = pd.DataFrame(scaler.transform(data_pred), columns=inputs, index=data_pred.index)

with open(f'{args.models}/FittedLabelEncoder.pkl', 'rb') as f:
    le = pickle.load(f)

for output in args.output_var:
    for i in range (len(d_discret)):
        try:
            model = load(f"{args.models}/{output}-{i}.joblib")
            data_pred[output] = model.predict(data_pred[inputs])
            if type(model) == type(XGBClassifier):
                le[output].inverse_transform(data_pred[output])
        except Exception as e:
            continue

if args.verbose:
    print(data_pred[args.output_var])

data_pred.to_csv(f"{args.output_path}/predicted_data.csv")

print(f"Preciction done ! Output is in {args.output_path}.\n")