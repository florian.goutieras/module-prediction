import pandas as pd
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from joblib import dump, load
import numpy as np
from extras import *
import pickle
import sklearn 
import xgboost as xgb
from sklearn.preprocessing import LabelEncoder

np.random.seed(seed=0)

# Import data
data = pd.read_csv("../data/data.csv", sep=";", encoding="utf-8", index_col=0)
data = data.drop(['granulo_span'], axis=1)

outliers = detect_outlier(data, 1, outputs)
data = data.drop(index=outliers)

for input in inputs:
    data = data.dropna(subset=[input])

data_dis=discretize(data, outputs, d_discret)[outputs]

data_stand=standardize_auto(data, inputs, scaler_filename=f"../scalers/scaler.joblib") # standardize data
scaler = load(f"../scalers/scaler.joblib")

scores = {}
dev = {}
params = {}
pred_val = {}
true_val = {}

start = time()

data_dis=discretize(data_stand, outputs, d_discret)

for model_name in list(classifiers): # for each model
    print(model_name+"\n")
    # (scores[model_name+f"-{i}"], dev[model_name+f"-{i}"], params[model_name+f"-{i}"], pred_val[model_name], true_val[model_name]) = evaluate_model_old(model_name, param_grids, data_dis, inputs, outputs, 1, 100)
    (scores[model_name+f"-{0}"], params[model_name+f"-{0}"], pred_val[model_name], true_val[model_name]) = evaluate_model(model_name, param_grids, data_dis, inputs, outputs)
print(f"\nJob done in {round(time() - start, 2)} seconds")

result_df = pd.DataFrame(columns=[outputs])
for key, values in scores.items():
    result_df.loc[key] = values

### Export pipelines pmml ###

from sklearn2pmml.pipeline import PMMLPipeline
from sklearn2pmml import sklearn2pmml

from joblib import dump, load
import pickle

scores=[]
le = {}

for output in outputs:

    best_model = result_df[output].idxmax().iloc[0]

    # faire les prédictions
    mdl = classifiers[best_model.split("-")[0]](**params[best_model])

    # print(f"{output}: {mdl}")

    scaler = load(f"../scalers/scaler.joblib")
    
    pipeline = PMMLPipeline([
        ("standardize", scaler),
        ("classifier", mdl),
    ])

    y = data_dis[output].dropna()
    X = data[inputs][data.index.isin(y.index)]

    if type(mdl) == type(XGBClassifier()):
        le[output] = LabelEncoder()
        # print(f"Best model for {output} is XGBClassifier")
        y = pd.Series(le[output].fit_transform(y))
    
    pipeline.fit(X,y)

    sklearn2pmml(pipeline, f'../models/{output}-{best_model.split("-")[-1]}-pipeline.pmml')

with open('../models/FittedLabelEncoder.pkl', 'wb') as f:
    pickle.dump(le, f)